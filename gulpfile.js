var gulp = require('gulp'),
    path = require('path'),
    less = require('gulp-less'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    lessDir = 'assets/less',
    stylesDir = 'assets/css',
    scriptsDir = 'assets/js';

/* 
 * build lib.css
 */
gulp.task('lib_styles', function () {
  return gulp.src([stylesDir+'/lib/bootstrap.css',
	stylesDir+'/lib/bootstrap-carousel-fade.css',
    stylesDir+'/lib/galleria.classic.css',
    stylesDir+'/lib/bigvideo.css',
	stylesDir+'/lib/animate.css',
	stylesDir+'/lib/slick.css',
	stylesDir+'/lib/hover.css'])
    .pipe(concat('lib.css'))
    .pipe(gulp.dest(stylesDir))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss())
    .pipe(gulp.dest(stylesDir));
});

/* 
 * build theme.less to theme.css and theme.min.css
 */
gulp.task('theme_styles', function () {
  return gulp.src(lessDir+'/theme.less')
    
    .pipe(less())
    .pipe(gulp.dest('.'))
    .pipe(rename({ suffix: '.min' }))
    
    .pipe(minifycss())
    .pipe(gulp.dest('.'));
});



/* 
 * build lib.js
 */
gulp.task('lib_scripts', function() {
  return gulp.src([
    scriptsDir + '/Lib/jquery-1.9.1.min.js',
	scriptsDir + '/Lib/bootstrap.min.js', 
    scriptsDir + '/Lib/holder.js',
    scriptsDir + '/Lib/jquery.ui.map.full.min.js', 
	scriptsDir + '/Lib/bootstrap-carousel.js', 
	scriptsDir + '/Lib/slick.min.js', 
    scriptsDir + '/Lib/galleria-1.2.9.js',
	scriptsDir + '/Lib/prettyCheckable.js',
    scriptsDir + '/Lib/chosen.jquery.min.js',
	scriptsDir + '/Lib/placeholders.min.js',
	scriptsDir + '/Lib/wow.js'])

    .pipe(concat('lib.js', {newLine: '\n'}))
    .pipe(gulp.dest(scriptsDir))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest(scriptsDir));
});

/* 
 * lint content.js and minify it to content.min.js
 * more info on js linting using jshint: http://jshint.com/docs/
 */
gulp.task('content_scripts', function() {
  return gulp.src(scriptsDir+'/content.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest(scriptsDir))
});


gulp.task('watch', function() {
  gulp.watch(lessDir+'/*.less', ['lib_styles']);
  gulp.watch('assets/less/*.less', ['theme_styles']);
 
  gulp.watch(scriptsDir + '/Lib/*.js', ['lib_scripts']);
  gulp.watch(scriptsDir+'/content.js', ['content_scripts']);

  
});

gulp.task('default', ['lib_styles', 'theme_styles', 'lib_scripts','content_scripts', 'watch']);